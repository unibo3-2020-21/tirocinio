﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Net.Http;



namespace combo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public enum state
        {
            INSERT,
            DELETE,
            MOVE
        }

#nullable enable
        private const double CircleWidth = 60;
        private static HttpClient client = new HttpClient();
        private static string requestDirUri = "api/products";

        private Ellipse? start = null;
        private Ellipse? end = null;
        private Line? actualLine = null;
        private state status = state.INSERT;

        private Dictionary<Ellipse, TouchPoint> nodes;
        private Dictionary<Ellipse, HashSet<Ellipse>> arches;


        public MainWindow()
        {
            
            InitializeComponent();
            client.BaseAddress = new Uri("http://c722-137-204-107-113.ngrok.io");
            nodes = new Dictionary<Ellipse, TouchPoint>();
            arches = new Dictionary<Ellipse, HashSet<Ellipse>>();
            
           
        }

        //Uncomment when working on NEXT STEP
        /*private void SwitchTouch(TouchPoint touchPoint)
        {
            switch (touchPoint.Action)
            {
                case TouchAction.Down:
                    ActionTouchDown(touchPoint);
                    break;
                case TouchAction.Move:
                    //Moving(_touchPoint);
                    break;
                default:
                    break;
            }
        }
        */

        private Ellipse AddEllipseAt(Canvas canv, Point pt, Brush brush)
        {
            Ellipse el = new Ellipse();
            el.Stroke = Brushes.AliceBlue;
            el.Fill = brush;
            el.Width = CircleWidth;
            el.Height = CircleWidth;
            el.Name = "Ellipse" + (this.nodes.Count + 1).ToString();
            this.RegisterName(el.Name, el);


            Canvas.SetLeft(el, pt.X - (CircleWidth / 2));
            Canvas.SetTop(el, pt.Y - (CircleWidth / 2));

            canv.Children.Add(el);

            return el;
        }


        private void ActionTouchDown(object sender, TouchEventArgs e)
        {
            TouchPoint touchPoint = e.GetTouchPoint(null);
            start = CheckEllipseInArea(touchPoint);
            // Uncomment when working on NEXT STEP
            //this.canvas.Children.Clear();
            /*if (this.insert.AreAnyTouchesOver)
            {
                status = state.INSERT;
                this.Status_TouchDown(touchPoint);
                return;
            }else if (this.delete.AreAnyTouchesOver)
            {
                status = state.DELETE;
                this.Status_TouchDown(touchPoint);
                return;
            }
            else if(this.move.AreAnyTouchesOver)
            {
                status  = state.MOVE;
                this.Status_TouchDown(touchPoint);
                return;
            }else*/
            if (start == null)
            {
                Ellipse el = AddEllipseAt(canvas, touchPoint.Position, Brushes.Red);
                Product p = new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 };
                HttpResponseMessage resp = client.PostAsJsonAsync(requestDirUri, new Tuple<Ellipse, TouchPoint>(el, touchPoint)).Result;
                resp.EnsureSuccessStatusCode();
                
                /*
                HttpResponseMessage resp1 = client.GetAsync("api/products").Result;
                resp1.EnsureSuccessStatusCode();

                IEnumerable<Product> products = resp.Content.ReadAsAsync<IEnumerable<Product>>().Result;
                //Debug.WriteLine(products);
                
                
                
                foreach (var pr in products)
                {
                    Debug.WriteLine("{0} {1} {2} ({3})", pr.Id, pr.Name, pr.Price, pr.Category);
                }*/
                nodes.Add(el, touchPoint);
                arches.Add(el, new HashSet<Ellipse>());
                this.start = el;
            }
        }
        
        private void DrawPermanentArch()
        {
            if (this.start != null && this.end != null && !this.start.Equals(end))
            {
                if (!arches[start].Contains(end))
                {
                    this.arches[this.start].Add(this.end);
                    this.arches[this.end].Add(this.start);
                    this.DrawTemporaryArch(this.nodes[this.start], this.nodes[this.end]);

                    this.PaintNodes();

                }

            }

        }

        private void DrawTemporaryArch(TouchPoint start, TouchPoint end)
        {
            Line _line = new Line();
            _line.Stroke = Brushes.White;
            _line.X1 = start.Position.X - 1;
            _line.X2 = end.Position.X - 1;
            _line.Y1 = start.Position.Y - 1;
            _line.Y2 = end.Position.Y - 1;
            _line.StrokeThickness = 2;
            this.actualLine = _line;
            this.canvas.Children.Add(this.actualLine);
        }

        private bool IsThereAButton(TouchPoint touched)
        {
            return true;
        }

        private Ellipse? CheckEllipseInArea(TouchPoint touched)
        {
            foreach (Ellipse el in nodes.Keys)
            {
                if (Math.Abs(nodes[el].Position.X - touched.Position.X) < 30 && Math.Abs(nodes[el].Position.Y - touched.Position.Y) < 30)
                {
                    Debug.WriteLine($"Found Ellipse N: {el.Name} ");
                    return el;
                }
            }
            return null;
        }

        private void Moving(object sender, TouchEventArgs e)
        {
            if (this.start != null)
            {
                try
                {
                    this.RemoveActualLine();
                }
                catch (NullReferenceException)
                {
                    Debug.WriteLine("null exception!");
                }
                DrawTemporaryArch(this.nodes[this.start], e.GetTouchPoint(null));
            }
        }

        private void MoveUp(object sender, TouchEventArgs e)
        {
            this.end = CheckEllipseInArea(e.GetTouchPoint(null));
            this.RemoveActualLine();
            DrawPermanentArch();
            ClearForNewArch();

        }

        private void PaintNodes()
        {
            this.canvas.Children.Remove(this.start);
            this.canvas.Children.Remove(this.end);
            this.canvas.Children.Add(this.start);
            this.canvas.Children.Add(this.end);
        }

        private void RemoveActualLine()
        {
            this.canvas.Children.Remove(this.actualLine);
        }

        private void ClearForNewArch()
        {
            this.start = null;
            this.end = null;
            this.actualLine = null;
        }

       /* private Ellipse? GetTouchedEllipse()
        {
            var tmp = nodes.Keys;

            foreach (var el in tmp)
            {
                //Debug.WriteLine($"for in ellipse: {el.Name}");
                if (el.AreAnyTouchesDirectlyOver) // OGNI TANTO (NON) FUNZIONA: SOLO IN CERTI PUNTI DELL'ELEMENTO MI DICE VERO CHE L'HO TOCCATO MENTRE IN ALTRI PUNTI (SEMPRE ALL'INTERNO DELL'OGGETTO) RITORNA FALSO
                {
                    //Debug.WriteLine($"captured touch on ellipse: {el.Name}");
                    return el;
                }
            }

            return null;
        }*/

        //NEXT STEP
        private void Status_TouchDown(TouchPoint touched)
        {
            // Button button = (Button) e.Source;
            touched.TouchDevice.Capture(canvas);

            switch (this.status)
            {
                case state.DELETE:
                    this.delete.Background = Brushes.Yellow;
                    this.insert.Background = Brushes.LightSlateGray;
                    this.move.Background = Brushes.LightSlateGray;
                    this.status = state.DELETE;
                    break;
                case state.INSERT:
                    this.delete.Background = Brushes.LightSlateGray;
                    this.insert.Background = Brushes.Yellow;
                    this.move.Background = Brushes.LightSlateGray;
                    this.status = state.INSERT;
                    break;
                case state.MOVE:
                    this.delete.Background = Brushes.LightSlateGray;
                    this.insert.Background = Brushes.LightSlateGray;
                    this.move.Background = Brushes.Yellow;
                    this.status = state.MOVE;
                    break;

            }
            ((MainWindow)Application.Current.MainWindow).UpdateLayout();
        }

        private void delete_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            Debug.WriteLine("button delete down");
            Button temp = (Button)sender;
            temp.Background = Brushes.Yellow;
            this.insert.Background = Brushes.LightSlateGray;
            this.move.Background = Brushes.LightSlateGray;
            this.status = state.DELETE;
            ((MainWindow)Application.Current.MainWindow).UpdateLayout();

        }

        private void insert_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            this.delete.Background = Brushes.LightSlateGray;
            Button temp = (Button)sender;
            temp.Background = Brushes.Yellow;
            this.move.Background = Brushes.LightSlateGray;
            this.status = state.INSERT;
            Debug.WriteLine("button insert down");
            ((MainWindow)Application.Current.MainWindow).UpdateLayout();
        }


        private void move_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            this.delete.Background = Brushes.LightSlateGray;
            this.insert.Background = Brushes.LightSlateGray;
            Button temp = (Button)sender;
            temp.Background = Brushes.Yellow;
            this.status = state.MOVE;
            Debug.WriteLine("button move down");
            ((MainWindow)Application.Current.MainWindow).UpdateLayout();
        }
    }
}