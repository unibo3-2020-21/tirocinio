﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Text.Json;
using System.Windows.Shapes;
using System.Windows.Input;

namespace ProductsApp.Controllers
{
    public class ProductsController : ApiController
    {
        private static List<Product> pp = new List<Product>();
        private static Dictionary<Ellipse, TouchPoint> nodes = new Dictionary<Ellipse, TouchPoint>();

        /*Product[] products = new Product[]
        {
            new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 },
            new Product { Id = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M },
            new Product { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M }
        };*/

        //EXAMPLE METHODS

        /*public void PostProduct(Product p)
        {
            pp.Add(p);
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return pp;
        }

        public IHttpActionResult GetProducts(int id)
        {
            var product = pp.FirstOrDefault((p) => p.Id == id);
            if(product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
        */

        //APP GRPH METHODS

        public void PostTuple(Tuple<Ellipse,TouchPoint> node)
        {
            nodes.Add(node.Item1, node.Item2);
        }
        
        /*
        public void PostNode(Ellipse node)
        {
            Console.WriteLine("post node");
        }
        public Dictionary<Ellipse, TouchPoint> GetAllNodes()
        {
            return nodes;
        }

        public IHttpActionResult GetTouchPoint(Ellipse id)
        {
            var touched = nodes[id];
            if (touched == null)
            {
                return NotFound();
            }
            return Ok(touched);
        }*/
    }
}